# Unofficial Proposals 

This is an unofficial repository for the purpose of developing unambigous design proposals for improvements to both open source and commercial Mattermost software. 

Depending on the outcome of discussions here, we may make this official, keep it unofficial, or close it. 